import {Injectable, Inject} from '@angular/core';
import {Restangular} from 'ngx-restangular';

@Injectable()
export class OrderApiService {

  constructor(@Inject(Restangular) public restangular) {
  }

  getTypes(appId) {
    return this.restangular.all(`app/${appId}/contacts`).getList();
  }

}
