import { RestangularExamplePage } from './app.po';

describe('restangular-example App', () => {
  let page: RestangularExamplePage;

  beforeEach(() => {
    page = new RestangularExamplePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
